#include <inttypes.h>
#include <math.h>
#include <stdlib.h>

#include "dsp.h"
#include "tables.h"
static void transpose_block(float *in_data, float *out_data)
{
  int i, j;
  int jAray[8] = { 0, 8, 16, 24, 32, 40, 48, 56 };
  for (i = 0; i < 8; ++i)
  {
	  int ofset = i * 8;
    for (j = 0; j < 8; ++j)
    {
      out_data[ofset+j] = in_data[jAray[j]+i];
    }
  }
}

static void neon_dct_1d(float *in_data, float *out_data)
{
	int i;
	for (i = 0; i < 8; ++i)
	{
		float32x4_t coeff_v1, coeff_v2, lookup1, lookup2;
		coeff_v1 = vld1q_f32(in_data);
		coeff_v2 = vld1q_f32(in_data + 4);
		float32x4_t dct_vec = vdupq_n_f32(0.0f);
		float dct = 0;
		lookup1 = vld1q_f32(dctlookup2[i]);
		lookup2 = vld1q_f32(dctlookup2[i] + 4);
		coeff_v1 = vmulq_f32(lookup1, coeff_v1);
		coeff_v2 = vmulq_f32(lookup2, coeff_v2);
		coeff_v1 = vaddq_f32(coeff_v1, coeff_v2);
		dct_vec = vaddq_f32(coeff_v1, dct_vec);
		dct = vaddvq_f32(dct_vec);
		out_data[i] = dct;
	}
}

static void neon_idct_1d(float *in_data, float *out_data)
{
	int i;
	for (i = 0; i < 8; ++i)
	{
		float32x4_t coeff_v1, coeff_v2, lookup1, lookup2;
		coeff_v1 = vld1q_f32(in_data);
		coeff_v2 = vld1q_f32(in_data + 4);
		float dct = 0;
		lookup1 = vld1q_f32(dctlookup[i]);
		lookup2 = vld1q_f32(dctlookup[i]+4);
		coeff_v1 = vmulq_f32(lookup1, coeff_v1);
		coeff_v2 = vmulq_f32(lookup2, coeff_v2);
		coeff_v1 = vaddq_f32(coeff_v1, coeff_v2);
		dct = vaddvq_f32(coeff_v1);
		out_data[i] = dct;
	}
}

static void neon_scale_block(float *in_data, float *out_data)
{
	int v;
	for (v = 0; v < 8; ++v)
	{
		int offset = v * 8;
		float32x4_t vec1  , vec2 ; // 4x4 vectors for input data
		float32x4_t outvec1 , outvec2 ; // 4x4 vector to store output
		float32x4_t lookupVec1 , lookupVec2 ; // 4x4 vector to store lookup values
		vec1 = vld1q_f32(in_data + offset);
		vec2 = vld1q_f32(in_data + offset + 4);
		lookupVec1 = vld1q_f32(dct_norm_table[v]);
		lookupVec2 = vld1q_f32(dct_norm_table[v] + 4);
		outvec1 = vmulq_f32(vec1, lookupVec1);
		outvec2 = vmulq_f32(vec2, lookupVec2);
		vst1q_f32(out_data+offset, outvec1);
		vst1q_f32(out_data + offset + 4, outvec2);
	}
}
static void quantize_block(float *in_data, float *out_data, uint8_t *quant_tbl)
{
  int zigzag;

  for (zigzag = 0; zigzag < 64; ++zigzag)
  {
    float dct = in_data[zigzagBlock[zigzag]];
    /* Zig-zag and quantize */
    out_data[zigzag] = (float) round((dct / 4.0) / quant_tbl[zigzag]);
  }
}

static void  neon_quantize_block(float *in_data, float *out_data, uint8_t *quant_tbl)
{
	int zigzag;
	float dct[64];
	for (zigzag = 0; zigzag < 64; ++zigzag)
	{
		dct[zigzag] = in_data[zigzagBlock[zigzag]] ;
	}
	for (zigzag = 0; zigzag < 8; ++zigzag)
	{
		int offset = zigzag * 8;
		float32x4_t vec1 = vld1q_f32(dct + offset);
		float32x4_t vec2 = vld1q_f32(dct + 4 + offset);
		float temp[8] = { quant_tbl[zigzag], quant_tbl[zigzag + 1], quant_tbl[zigzag + 2], quant_tbl[zigzag + 3], quant_tbl[zigzag + 4], quant_tbl[zigzag + 5], quant_tbl[zigzag + 6], quant_tbl[zigzag + 7] };
		float32x4_t quantvec1 = vld1q_f32(temp);
		float32x4_t quantvec2 = vld1q_f32(temp + 4);
		float32x4_t const4 = vdupq_n_f32(4.0f);
		quantvec1 = vmulq_f32(quantvec1, const4);
		quantvec2 = vmulq_f32(quantvec2, const4);
		float32x4_t ans1, ans2;
		ans1 = vdivq_f32(vec1, quantvec1);
		ans2 = vdivq_f32(vec2, quantvec2);
		vst1q_f32(out_data + offset, vrndq_f32(ans1));
		vst1q_f32(out_data + offset + 4, vrndq_f32(ans2));
	}
}

static void dequantize_block(float *in_data, float *out_data,
    uint8_t *quant_tbl)
{
  int zigzag;

  for (zigzag = 0; zigzag < 64; ++zigzag)
  {
    float dct = in_data[zigzag];
    /* Zig-zag and de-quantize */
	out_data[zigzagBlock[zigzag]] = (float)round((dct * quant_tbl[zigzag]) / 4.0);
  }
}

static void  neon_dequantize_block(float *in_data, float *out_data,
	uint8_t *quant_tbl)
{
	int zigzag;
	float ans[64];
	for (zigzag = 0; zigzag < 8; ++zigzag)
	{
		int offset = zigzag * 8;
		float32x4_t vec1 = vld1q_f32(in_data + offset);
		float32x4_t vec2 = vld1q_f32(in_data + 4 + offset);
		float temp[8] = { quant_tbl[zigzag] , quant_tbl[zigzag + 1] , quant_tbl[zigzag + 2] , quant_tbl[zigzag + 3] , quant_tbl[zigzag + 4] , quant_tbl[zigzag + 5] , quant_tbl[zigzag + 6] , quant_tbl[zigzag + 7]  };
		float32x4_t quantvec1 = vld1q_f32(temp);
		float32x4_t quantvec2 = vld1q_f32(temp + 4);
		vec1 = vmulq_f32(vec1, vdivq_f32(quantvec1, vdupq_n_f32(4.0f)));
		vec2 = vmulq_f32(vec2, vdivq_f32(quantvec2, vdupq_n_f32(4.0f)));
		vst1q_f32(ans + offset, vec1);
		vst1q_f32(ans + offset + 4, vec2);
	}
	for (zigzag = 0; zigzag < 64; ++zigzag)
	{
		out_data[zigzagBlock[zigzag]] = ans[zigzag];
	}
}
void dct_quant_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl)
{
  float mb[8*8] __attribute((aligned(16)));
  float mb2[8*8] __attribute((aligned(16)));

  int i, v;

  for (i = 0; i < 64; ++i) { mb2[i] = in_data[i]; }

  /* Two 1D DCT operations with transpose */
  for (v = 0; v < 8; ++v) { neon_dct_1d(mb2 + v * 8, mb + v * 8); }
  transpose_block(mb, mb2);
  for (v = 0; v < 8; ++v) { neon_dct_1d(mb2 + v * 8, mb + v * 8); }
  transpose_block(mb, mb2);

  neon_scale_block(mb2, mb);
  quantize_block(mb, mb2, quant_tbl);

  for (i = 0; i < 64; ++i) { out_data[i] = mb2[i]; }
}

void dequant_idct_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl)
{
  float mb[8*8] __attribute((aligned(16)));
  float mb2[8*8] __attribute((aligned(16)));

  int i, v;

  for (i = 0; i < 64; ++i) { mb[i] = in_data[i]; }

  dequantize_block(mb, mb2, quant_tbl);
  neon_scale_block(mb2, mb);

  /* Two 1D inverse DCT operations with transpose */
  for (v = 0; v < 8; ++v) { neon_idct_1d(mb+v*8, mb2+v*8); }
  transpose_block(mb2, mb);
  for (v = 0; v < 8; ++v) { neon_idct_1d(mb+v*8, mb2+v*8); }
  transpose_block(mb2, mb);

  for (i = 0; i < 64; ++i) { out_data[i] = mb[i]; }
}


void neon_sad_block_8x8(uint8_t *block1, uint8_t *block2, int stride, int *result)
{
	int v;

	*result = 0;
	for (v = 0; v < 4; ++v)
	{
		int offset = v*stride;
		int offset1 = (v + 1)*stride;
		uint8x16_t vec1, vec2, absdiff;
		vec1 = vcombine_u8(vld1_u8(block1 + offset), vld1_u8(block1 + offset1)); //loading first two rows of block1
		vec2 = vcombine_u8(vld1_u8(block2 + offset), vld1_u8(block2 + offset1)); //loading first two rows of block2
		absdiff = vabdq_u8(vec2, vec1); // 16 parallel absolute difference calculations
		result+=vaddlvq_u8(absdiff);
	}
}