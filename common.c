#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "dsp.h"


void neon_dequantize_idct_row(int16_t *in_data, uint8_t *prediction, int w, int h,
	int y, uint8_t *out_data, uint8_t *quantization)
{
	int x;

	int16_t block[8 * 8];

	/* Perform the dequantization and iDCT */
	for (x = 0; x < w; x += 8)
	{
		int i;

		dequant_idct_block_8x8(in_data + (x * 8), block, quantization);

		for (i = 0; i < 8; ++i)
		{
			int block_offset = i * 8;
			int prediction_offset = i*w + x;
			int16x8_t block_vec, prediction_vec;
			//casting uint8 vector to int16 vector
			prediction_vec = vreinterpretq_s16_u16(vmovl_u8(vld1_u8(prediction + prediction_offset)));
			block_vec = vld1q_s16(block + block_offset);
			int16x8_t tmp = vaddq_s16(block_vec, prediction_vec); // vector addition
			// take the parallel maximum between your value and zero.
			int16x8_t allZero = vdupq_n_s16(0); // all element 0 vector
			int16x8_t all255 = vdupq_n_s16(255); // all element 255 vector
			tmp = vmaxq_s16(tmp, allZero);    // set -ve values to 0
			tmp = vminq_s16(tmp, all255);	  // set +ve values > 255 to 255
			// casting to uint8 vector
			uint8x8_t casted= vreinterpret_u8_s8(vmovn_s16(tmp));
			vst1_u8(out_data + prediction_offset , casted); //result storage
		}
	}
}

void dequantize_idct(int16_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, uint8_t *out_data, uint8_t *quantization)
{
  int y;

  for (y = 0; y < height; y += 8)
  {
    neon_dequantize_idct_row(in_data+y*width, prediction+y*width, width, height, y,
        out_data+y*width, quantization);
  }
}


void neon_dct_quantize_row(uint8_t *in_data, uint8_t *prediction, int w, int h,
	int16_t *out_data, uint8_t *quantization)
{
	int x;

	int16_t block[8 * 8];

	/* Perform the DCT and quantization */
	for (x = 0; x < w; x += 8)
	{
		int i;

		for (i = 0; i < 8; ++i)
		{
			int16x8_t in_vec,prediction_vec;
			int block_offset=i*8;
			int prediction_offset = i*w + x;
			//casting uint8 vector to int16 vector
			in_vec = vreinterpretq_s16_u16(vmovl_u8(vld1_u8(in_data + prediction_offset)));
			prediction_vec = vreinterpretq_s16_u16(vmovl_u8(vld1_u8(prediction + prediction_offset))); 
			int16x8_t result = vsubq_s16(in_vec, prediction_vec); //vector substraction
			vst1q_s16(block+block_offset, result); //result storage
		}

		/* Store MBs linear in memory, i.e. the 64 coefficients are stored
		continous. This allows us to ignore stride in DCT/iDCT and other
		functions. */
		dct_quant_block_8x8(block, out_data + (x * 8), quantization);
	}
}
void dct_quantize(uint8_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, int16_t *out_data, uint8_t *quantization)
{
  int y;

  for (y = 0; y < height; y += 8)
  {
    neon_dct_quantize_row(in_data+y*width, prediction+y*width, width, height,
        out_data+y*width, quantization);
  }
}

void destroy_frame(struct frame *f)
{
  /* First frame doesn't have a reconstructed frame to destroy */
  if (!f) { return; }

  free(f->recons->Y);
  free(f->recons->U);
  free(f->recons->V);
  free(f->recons);

  free(f->residuals->Ydct);
  free(f->residuals->Udct);
  free(f->residuals->Vdct);
  free(f->residuals);

  free(f->predicted->Y);
  free(f->predicted->U);
  free(f->predicted->V);
  free(f->predicted);

  free(f->mbs[Y_COMPONENT]);
  free(f->mbs[U_COMPONENT]);
  free(f->mbs[V_COMPONENT]);

  free(f);
}

struct frame* create_frame(struct c63_common *cm, yuv_t *image)
{
  struct frame *f = malloc(sizeof(struct frame));

  f->orig = image;

  f->recons = malloc(sizeof(yuv_t));
  f->recons->Y = malloc(cm->ypw * cm->yph);
  f->recons->U = malloc(cm->upw * cm->uph);
  f->recons->V = malloc(cm->vpw * cm->vph);

  f->predicted = malloc(sizeof(yuv_t));
  f->predicted->Y = calloc(cm->ypw * cm->yph, sizeof(uint8_t));
  f->predicted->U = calloc(cm->upw * cm->uph, sizeof(uint8_t));
  f->predicted->V = calloc(cm->vpw * cm->vph, sizeof(uint8_t));

  f->residuals = malloc(sizeof(dct_t));
  f->residuals->Ydct = calloc(cm->ypw * cm->yph, sizeof(int16_t));
  f->residuals->Udct = calloc(cm->upw * cm->uph, sizeof(int16_t));
  f->residuals->Vdct = calloc(cm->vpw * cm->vph, sizeof(int16_t));

  f->mbs[Y_COMPONENT] =
    calloc(cm->mb_rows * cm->mb_cols, sizeof(struct macroblock));
  f->mbs[U_COMPONENT] =
    calloc(cm->mb_rows/2 * cm->mb_cols/2, sizeof(struct macroblock));
  f->mbs[V_COMPONENT] =
    calloc(cm->mb_rows/2 * cm->mb_cols/2, sizeof(struct macroblock));

  return f;
}

void dump_image(yuv_t *image, int w, int h, FILE *fp)
{
  fwrite(image->Y, 1, w*h, fp);
  fwrite(image->U, 1, w*h/4, fp);
  fwrite(image->V, 1, w*h/4, fp);
}
